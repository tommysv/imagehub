# ASP .NET-based photoalbum with categories #

For demonstrational purposes only

![imagehub.jpg](https://bitbucket.org/repo/yppMoz4/images/3022714731-imagehub.jpg)


## Functions: ##

* Register/login users who can upload pictures
* Upload images in .jpg- or .png-format (max. 2 mb)
* Show latest uploaded images
* Show images in predefined categories

Technologies used: Code-first, Visual Studio 2015, C#, ASP .NET MVC 5, .NET Framework 4.5, Entity Framework 6, Bootstrap