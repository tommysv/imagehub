using ImageHub.Core.Repositories;

namespace ImageHub.Core
{
    public interface IUnitOfWork
    {
        IFileRepository FileRepository { get; }
        IImageCatalogRepository ImageCatalog { get; }
        void SaveChanges();
    }
}