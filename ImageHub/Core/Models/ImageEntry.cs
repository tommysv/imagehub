﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImageHub.Core.Models
{
    public class ImageEntry
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string FilenameBase { get; set; }
       
        [Required]
        [StringLength(5)]
        public string FileExt { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public ImageCategory ImageCategory { get; set; }

        public DateTime Uploaded { get; set; }

        public DateTime Modified { get; set; }

        [Required]
        [StringLength(128)]
        public string Uploader { get; set; }
    }
}