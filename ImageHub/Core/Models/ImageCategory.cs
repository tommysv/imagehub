﻿using System.ComponentModel.DataAnnotations;

namespace ImageHub.Core.Models
{
    public class ImageCategory
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}