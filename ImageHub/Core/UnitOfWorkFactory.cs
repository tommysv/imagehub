﻿using System;

namespace ImageHub.Core
{
    public class UnitOfWorkFactory
    {
        public static IUnitOfWork Create()
        {
            Type type = Type.GetType("ImageHub.Persistence.UnitOfWork");
            return (IUnitOfWork)Activator.CreateInstance(type);
        }
    }
}