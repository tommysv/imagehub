﻿using System;
using System.Web;

namespace ImageHub.Core.Repositories
{
    public interface IFileRepository
    {
        string GenerateFilenameBase(DateTime dateTime);
        void SaveImage(string mapPath, string filenameBase, HttpPostedFileBase imageData);
        void SaveThumbnailImage(string mapPath, string filenameBase, HttpPostedFileBase imageData);
    }
}