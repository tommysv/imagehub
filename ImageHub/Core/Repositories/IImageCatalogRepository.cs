using ImageHub.Core.Models;
using System.Collections.Generic;

namespace ImageHub.Core.Repositories
{
    public interface IImageCatalogRepository
    {
        void AddImage(ImageEntry imageEntry);
        IEnumerable<ImageEntry> GetImagesByCategory(int? id);
        ImageCategory GetCategoryById(int? id);
        List<ApplicationUser> GetUserEntries();
        IEnumerable<ImageEntry> GetLatestImages();
        IEnumerable<ImageCategory> GetCategories();
        ImageEntry GetImageById(int id);
        void RemoveImage(ImageEntry imageEntry);
    }
}