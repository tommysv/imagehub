using System.ComponentModel.DataAnnotations;

namespace ImageHub.Core.ViewModels.Account
{

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

}