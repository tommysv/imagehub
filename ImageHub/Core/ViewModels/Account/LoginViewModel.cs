using System.ComponentModel.DataAnnotations;

namespace ImageHub.Core.ViewModels.Account
{

    public class LoginViewModel
    {
        [Required(ErrorMessage="Email is a required field")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is a required field")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

}