﻿using System.Collections.Generic;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class ImageCatalogViewModel
    {
        public List<ImageEntryViewModel> ImageEntries { get; set; }
    }
}