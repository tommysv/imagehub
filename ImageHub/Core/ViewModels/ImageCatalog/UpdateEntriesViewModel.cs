﻿using System.Collections.Generic;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class UpdateEntriesViewModel
    {
        public IEnumerable<UpdateEntryViewModel> UpdateEntries { get; set; }
    }
}