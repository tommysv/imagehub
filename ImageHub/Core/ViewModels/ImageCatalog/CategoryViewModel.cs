﻿using System.Collections.Generic;

namespace ImageHub.Core.ViewModels.ImageCatalog
{
    public class CategoryViewModel
    {
        public string CategoryName { get; set; }
        public IEnumerable<ImageEntryViewModel> ImageEntries { get; set; }
    }
}