﻿using ImageHub.Core.Models;
using ImageHub.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace ImageHub.Persistence.Repositories
{
    public class ImageCatalogRepository : IImageCatalogRepository
    {
        private readonly IApplicationDbContext _context;

        public ImageCatalogRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public void AddImage(ImageEntry imageEntry)
        {
            _context.ImageEntries.Add(imageEntry);
        }


        public IEnumerable<ImageEntry> GetImagesByCategory(int? id)
        {
            return _context.ImageEntries
                .Where(m => m.CategoryId == id)
                .OrderByDescending(m => m.Uploaded)
                .AsEnumerable();
        }

        public ImageCategory GetCategoryById(int? id)
        {
            return _context.ImageCategories.SingleOrDefault(m => m.Id == id);
        }

        public List<ApplicationUser> GetUserEntries()
        {
            return _context.Users.ToList();
        }

        public IEnumerable<ImageEntry> GetLatestImages()
        {
            return _context.ImageEntries
                .OrderByDescending(m => m.Uploaded)
                .Take(10)
                .AsEnumerable();
        }

        public IEnumerable<ImageCategory> GetCategories()
        {
            return _context.ImageCategories.ToList();
        }

        public ImageEntry GetImageById(int id)
        {
            return _context.ImageEntries.SingleOrDefault(m => m.Id == id);
        }

        public void RemoveImage(ImageEntry imageEntry)
        {
            _context.ImageEntries.Remove(imageEntry);
        }
    }
}