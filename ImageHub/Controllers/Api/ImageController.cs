﻿using ImageHub.Core;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace ImageHub.Controllers.Api
{
    public class ImageController : ApiControllerBase
    {
        public ImageController()
            : base(UnitOfWorkFactory.Create())
        {
        }

        [HttpDelete]
        [Authorize]
        [Route("api/image/{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized();

            var currentUserId = User.Identity.GetUserId();
            var imageEntry = UnitOfWork.ImageCatalog.GetImageById(id);
            if (imageEntry == null)
                return NotFound();

            if (currentUserId != imageEntry.Uploader)
                return Unauthorized();

            UnitOfWork.ImageCatalog.RemoveImage(imageEntry);
            UnitOfWork.SaveChanges();

            return Ok();
        }
    }
}
