﻿using ImageHub.Core;
using System.Web.Http;

namespace ImageHub.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public ApiControllerBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
            
        }
    }
}